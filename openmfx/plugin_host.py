import logging

from ctypes import pointer, byref


from .utils import _indent, _outdent, print_indent

from .openmfx import OfxPluginBinary

from .property_set import PropertySetCollection

from .generated.ofxCore import OfxPropertySetStruct, OfxPlugin, OfxHost


class OpenMfxPluginHost:

    _host_property_set_collection = None
    _host_property_set = None

    # Note: Current implementation only supports a single plugin with a single instance.
    # TODO: Change this?
    _ofx_plugin_binary = None
    _ofx_plugin = None


    def __init__(self):

        self._host_property_set_collection = PropertySetCollection()
        self._host_property_set = OfxPropertySetStruct()

        self._host_property_set_collection.add(self._host_property_set, tag="host_property_set")



    def load_plugin(self, plugin_file_path, plugin_index):

        self._ofx_plugin_binary = OfxPluginBinary.load_from_path(plugin_file_path)

        # TODO: Validate `plugin_index`.

        self._ofx_plugin = OfxPlugin.from_address(self._ofx_plugin_binary.plugin_binary.OfxGetPlugin(plugin_index))

        # TODO: Validate `pluginApi` & `apiVersion` values.

        logging.info("proceeding with plugin #%d (%s / %s)", plugin_index, self._ofx_plugin.pluginIdentifier, self._ofx_plugin.pluginApi)


        host_info = OfxHost()
        host_info.host = pointer(self._host_property_set) #?

        #
        # Originally I used annotation of the form:
        #
        #     OFX_HOST_FETCH_SUIT_FUNC_TYPE = ofxCore.OfxHost._fields_[1][1]
        #
        #     @OFX_HOST_FETCH_SUIT_FUNC_TYPE
        #     def fetch_suite(host, suite_name, suite_version):
        #         # ...
        #
        # Which obviously isn't ideal given the hardcoded array indexes.
        #
        # Other occurrences used an approach more like this:
        #
        #     OFX_HOST_FETCH_SUIT_FUNC_TYPE = OfxHost().fetchSuite.__class__
        #     # ...
        #
        # Which isn't *great* but I'm not sure if there's a way to get
        # the function prototype without creating an instance.
        #
        # (Subsequently I discovered use of `type()` was a nicer way
        # to access the value in `__class__` via <https://docs.python.org/3/library/functions.html?highlight=__class__#type>.)
        #
        #
        # One other issue I ran into I think was the original reason for the
        # intermediate `OFX_HOST_FETCH_SUIT_FUNC_TYPE` variable:
        #
        # Namely, Python wouldn't permit an annotation that directly used the
        # original form of the annotation with the array access, i.e.:
        #
        #     @OfxHost._fields_[1][1]
        #
        # It turns out this was an intentional limitation of the original
        # annotation syntax implementation which was removed in Python 3.9
        # release. (See: <https://peps.python.org/pep-0614/>)
        #
        # But given the new `@type(OfxHost().fetchSuite)` approach it ended
        # up not being an issue.
        #
        # But for backward compatibility with older versions before finding
        # the new approach I tried a couple of hacky workarounds:
        #
        #     @eval("OfxHost().fetchSuite.__class__")
        #
        #     @host_info.fetchSuite.__class__
        #
        # (Not pictured here: "identity" function described in above PEP.)
        #

        @type(OfxHost().fetchSuite)
        def fetch_suite(host, suite_name, suite_version):
            print_indent("fetch_suite: ", host, suite_name, suite_version)
            print_indent("property set:", self._host_property_set_collection.get(host.contents))


        host_info.fetchSuite = fetch_suite


        _indent("pre-`setHost(...)` call...")

        # Note: Plugins *shouldn't* call OFX functions within this but... :D
        #
        #       See: <https://github.com/eliemichel/OpenMfx/blob/9d50a87eae30ccd37bf60ad21ba01f68693e90ce/include/ofxCore.h#L148>

        self._ofx_plugin.setHost(byref(host_info))

        _outdent("post-`setHost(...)` call...")
