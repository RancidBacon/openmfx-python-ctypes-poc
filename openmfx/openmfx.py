#
# Higher level wrapper for OpenMfx/OpenFx plugin interaction.
#

from ctypes import *

from .generated import ofxCore

import os
import logging

# via <https://gist.github.com/juanpabloaj/3e6a41f683c1767c17824811db01165b>
LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logging.basicConfig(level=LOGLEVEL, format=logging.BASIC_FORMAT.replace(":%(message)", ": %(message)"))


class OfxPluginBinary:

    plugin_binary = None

    _num_plugins = 0


    @staticmethod
    def load_from_path(file_path: str):

        logging.info(f"plugin binary file path: {file_path}")

        the_plugin_binary = OfxPluginBinary()
        the_plugin_binary.plugin_binary = ofxCore.load_library(file_path)

        logging.debug(f"loaded plugin binary: {the_plugin_binary.plugin_binary}")

        the_plugin_binary._apply_workaround_fixups()

        return the_plugin_binary



    def _apply_workaround_fixups(self):

        #
        # This works around an apparent issue with ctypes (?)
        # where a return type gets handle incorrectly (?).
        #
        # This results in a segfault in some situations with
        # some plugins (on some OS?).
        #
        #
        # The issue AIUI is that a pointer gets treated as a
        # (signed?) integer and/or gets truncated to half its
        # width or something... :D
        #

        logging.debug("applying `OfxGetPlugin` return type fixup:")
        logging.debug("  (pre-fixup) `OfxGetPlugin.restype`: %s" % self.plugin_binary.OfxGetPlugin.restype)

        self.plugin_binary.OfxGetPlugin.restype = c_void_p

        logging.debug("  (post-fixup) `OfxGetPlugin.restype`: %s" % self.plugin_binary.OfxGetPlugin.restype)


    @property
    def num_plugins(self) -> int:
        return self.get_num_plugins()


    def get_num_plugins(self) -> int:

        # TODO: Handle better?
        if not self._num_plugins:
            self._num_plugins = self.plugin_binary.OfxGetNumberOfPlugins()
            logging.info(f"number of plugins in plugin binary: {self._num_plugins}")

        return self._num_plugins


# ---------------------------


def dump_ofx_plugin_info(file_path):

    print()
    print("plugin binary file path:", file_path)

    ofx_plugin_binary = OfxPluginBinary.load_from_path(file_path)

    print("loaded plugin binary:", ofx_plugin_binary.plugin_binary)
    print()

    return dump_ofx_plugin_info_for_binary(ofx_plugin_binary)



def dump_ofx_plugin_info_for_binary(the_plugin_binary):

    num_plugins = the_plugin_binary.get_num_plugins()

    print("num_plugins: %d" % num_plugins)
    print()


    plugin_binary = the_plugin_binary.plugin_binary # Temporary refactoring hack.


    for current_plugin_idx in range(num_plugins):

        print()
        print("plugin #%d" % current_plugin_idx)
        print()

        ofx_plugin_address = plugin_binary.OfxGetPlugin(current_plugin_idx)

        print('    [debug: `ofx_plugin_address` (ensure not truncated): {:#018x}]'.format(ofx_plugin_address))

##        ofx_plugin = ofxCore.OfxPlugin.from_address(ofx_plugin_address)
        ofx_plugin = ofxCore.struct_OfxPlugin.from_address(ofx_plugin_address)

        print()
        print("    plugin instance:", ofx_plugin)
        print()

        for (field_name, _) in ofx_plugin._fields_:

            print("    %s: %s" % (field_name, getattr(ofx_plugin, field_name)))

        print()

    return the_plugin_binary
