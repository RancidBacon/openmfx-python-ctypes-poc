#
# Helper utility functions not OpenMfx-specific.
#

import logging



## ---------------------------------------------------

import inspect

import textwrap
from pprint import pprint, pformat


indent = "   "


def printi(*args):

    # via <https://stackoverflow.com/questions/5067604/determine-function-name-from-within-that-function-without-using-traceback>
    caller_fn_name = inspect.currentframe().f_back.f_code.co_name

    print()
    print(indent, caller_fn_name, ":", *args)


def print_indent(*args):

    logging.debug(indent + "%s", " ".join([str(a) for a in args]))


def print_indent_dict(d: dict):

    global indent

    indent += "    "
    print()
    print(textwrap.indent(pformat(d), indent))
    indent = indent[0:-4]


def _indent(msg):

    global indent

    print_indent(msg)
    indent += "    "


def _outdent(msg):

    global indent

    indent = indent[0:-4]

    print_indent(msg)
