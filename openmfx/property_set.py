from ctypes import addressof

from .utils import *
from .utils import _indent, _outdent


class PropertySetWrapper:

    _struct = None
    _values_dict = {}

    _handle = None

    _tag = ""

    def __init__(self, the_property_set_struct, tag=""):

        self._struct = the_property_set_struct

        self._handle = addressof(the_property_set_struct)

        self._values_dict = {}

        self._tag = tag

        self._values_dict["__meta_info"] = {'tag': self._tag, 'handle': self._handle, 'handle_hex': hex(self._handle)}



    def __repr__(self):
        return "%s tag='%s'>" % (super().__repr__()[:-1], self._tag)




class PropertySetCollection:

    _property_sets = {}


    def add(self, the_property_set, tag=""):  # NOTE: Now returns handle. # TODO: Tidy all this, e.g. direct vs via pointer access.

        _indent("")
        print_indent(f"PropertySetCollection::`add` to collection with wrap and tag: `{tag}`")

        wrapped_property_set = PropertySetWrapper(the_property_set, tag)

        self._property_sets[wrapped_property_set._handle] = wrapped_property_set

        _outdent("")

        return wrapped_property_set._handle  # See note above. Also: Maybe return wrapped property set directly?



    def _get_by_handle(self, handle):

        _indent("")
        print_indent("PropertySetCollection::`_get` via supplied handle:", handle, hex(handle))
        _outdent("")

        return self._property_sets[handle]



    def get(self, the_property_set):

        _indent("")
        print_indent("PropertySetCollection::`get` via `addressof()`:", addressof(the_property_set), hex(addressof(the_property_set)))
        _outdent("")

        return self._property_sets[addressof(the_property_set)]



    def exists(self, the_property_set):

        _indent("")
        print_indent("exists?", addressof(the_property_set), hex(addressof(the_property_set)))
        _outdent("")

        return addressof(the_property_set) in self._property_sets



    def hack_add(self, the_property_set, handle, tag="hack_add@???"):

        _indent("")
        print()
        print_indent("[`hack_add`] wrapping property set & (hack) adding to collection with tag:", tag, " and supplied handle:", handle)

        wrapped_property_set = PropertySetWrapper(the_property_set, tag)

        wrapped_property_set._handle = handle

        self._property_sets[wrapped_property_set._handle] = wrapped_property_set

        print()
        _outdent("")
