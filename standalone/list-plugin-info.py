#!/usr/bin/python3

#
# An initial hand-crafted `.ofx` info dumper.
#
# (Hand-crafted in the sense that it doesn't use generated
# bindings, it just uses manually created `ctypes` calls.)
#

import os

import ctypes
from ctypes import *


# via <https://github.com/eliemichel/OpenMfx/blob/9d50a87eae30ccd37bf60ad21ba01f68693e90ce/include/ofxCore.h#L117>
class OfxPlugin(Structure):
    _fields_ = \
        [
            ("pluginApi", ctypes.c_char_p),
            ("apiVersion", ctypes.c_int),
            ("pluginIdentifier", ctypes.c_char_p),
            # ...
        ]



def dump_ofx_plugin_info(file_path):

    print()
    print("plugin binary file path:", file_path)

    ofx_plugin_binary = None

    if os.name == "nt":
        ofx_plugin_binary = windll.LoadLibrary(file_path)
    else:
        ofx_plugin_binary = cdll.LoadLibrary(file_path)

    print("loaded plugin binary:", ofx_plugin_binary)

    if ofx_plugin_binary is None:

        print("failed to load.")
        print()

        return


    print()

    # via <https://github.com/eliemichel/OpenMfx/blob/9d50a87eae30ccd37bf60ad21ba01f68693e90ce/include/ofxCore.h#L581>
    num_plugins = ofx_plugin_binary.OfxGetNumberOfPlugins()

    print("num_plugins: %d" % num_plugins)
    print()


    #
    # This works around an apparent issue with ctypes (?)
    # where a return type gets handle incorrectly (?).
    #
    # This results in a segfault in some situations with
    # some plugins (on some OS?).
    #
    #
    # The issue AIUI is that a pointer gets treated as a
    # (signed?) integer and/or gets truncated to half its
    # width or something... :D

    ofx_plugin_binary.OfxGetPlugin.restype = c_void_p


    for current_plugin_idx in range(num_plugins):

        print()
        print("plugin #%d" % current_plugin_idx)


        ## via <https://github.com/eliemichel/OpenMfx/blob/9d50a87eae30ccd37bf60ad21ba01f68693e90ce/include/ofxCore.h#L573>
        # ofx_plugin_struct = OfxPlugin.from_address(ofx_plugin_binary.OfxGetPlugin(current_plugin_idx))

        ofx_plugin_struct_address = ofx_plugin_binary.OfxGetPlugin(current_plugin_idx)
        ofx_plugin_struct = OfxPlugin.from_address(ofx_plugin_struct_address)

        for (field_name, _) in OfxPlugin._fields_:
            print("    %s: %s" % (field_name, getattr(ofx_plugin_struct, field_name)))

    print()



if __name__ == "__main__":

    import sys

    ofx_plugin_file_path = sys.argv[1]

    dump_ofx_plugin_info(ofx_plugin_file_path)
