#!/usr/bin/python3

from openmfx import OpenMfxPluginHost


if __name__ == "__main__":

    import sys

    ofx_plugin_file_path = sys.argv[1]
    selected_plugin_idx = int(sys.argv[2])

    print()
    print(f"plugin file path: {ofx_plugin_file_path}")
    print(f"plugin index: {selected_plugin_idx}")
    print()

    plugin_host = OpenMfxPluginHost()

    plugin_host.load_plugin(ofx_plugin_file_path, selected_plugin_idx)
