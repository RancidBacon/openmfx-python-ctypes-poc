#!/usr/bin/python3

import openmfx

if __name__ == "__main__":

    import sys

    ofx_plugin_file_path = sys.argv[1]

    openmfx.dump_ofx_plugin_info(ofx_plugin_file_path)
