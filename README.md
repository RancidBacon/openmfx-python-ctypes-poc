## OpenMfx / OpenFX Python host via `ctypes` proof-of-concept (public)

Or, you know, you could just go here instead: <https://github.com/eliemichel/PythonMfx/>

[TODO]

### Examples

#### `list-plugin-info.py`

This is a standalone example that does not require the generated bindings.

Usage:

```bash
./standalone/list-plugin-info.py <plugin.ofx>
```

"Tested" with these OpenMfx/OpenFx plugins:

 * `openmfx_test_parameters_plugin.ofx`
 * `libmfx_examples.ofx` ([via](https://github.com/eliemichel/MfxExamples))
 * `openmfx_sample_plugin.ofx`
 * via [Natron v2.4.3](https://github.com/NatronGitHub/Natron/releases/tag/v2.4.3)
    * `Arena.ofx` ([see also](https://github.com/NatronGitHub/openfx-arena))
    * `Misc.ofx` ([see also](https://github.com/NatronGitHub/openfx-misc))


#### `dump-plugin-info.py`

This uses the generated bindings in the `openmfx` library.

Usage:

```bash
./dump-plugin-info.py <plugin.ofx>
```
